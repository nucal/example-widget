/**
 * if you don't give
 * .android.js, .ios.js, .native.js, .js or .vue extension,
 * vuexp will choose related file for each platform
 */
import CheckboxWidget from './checkbox-widget';

export default {
  install(Vue, options) {
    Vue.registerElement('CheckboxWidget', () => CheckboxWidget, {
      model: {},
    });
  },
};
